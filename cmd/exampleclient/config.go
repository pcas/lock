// Config.go handles configuration and logging for exampleclient.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/config"
	"bitbucket.org/pcas/config/env"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"fmt"
	"github.com/pkg/errors"
	"os"
)

// Options describes the options.
type Options struct {
	Host string // The hostname of the lock server
	Port int    // The port number of the lock server
}

// Name is the name of the executable.
const Name = "exampleclient"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := &Options{}
	// Parse the optional flags
	parseFlags(opts)
	// Pull in and validate the configuration information
	assertNoErr(validateEnvironment(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "lockclient: %s\n", err)
		os.Exit(1)
	}
}

// parseFlags parses the command-line flags.
func parseFlags(opts *Options) {
	var verbose bool
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf(`%s is a simple client for the pcas lockd server.

Usage: %s [options] `, Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.Bool("v", &verbose, verbose, "Increase verbosity", ""),
	)
	flag.SetUsageFooter(`The following environment variables must be set:
		
  PCAS_LOCK_ADDRESS   The address of the pcas lock server, in the form hostname:port.`)
	// Parse the flags
	flag.Parse()
	// Set the logger
	if verbose {
		log.SetLogger(log.Stderr)
	}
}

// validateEnvironment pulls in configuration information from the environment and validates it.
func validateEnvironment(opts *Options) error {
	// Read in the environment configuration information
	cfg := env.EnvConfig(nil)
	// Validate the address of the lock server
	address, err := config.GetStringOrError(cfg, "lock.address")
	if err != nil {
		return errors.New("The environment variable PCAS_LOCK_ADDRESS must be set to the address of the pcas lock server")
	}
	var hasPort bool
	hasPort, opts.Host, opts.Port, err = flag.ParseAddress(address)
	if err != nil {
		return errors.Wrap(err, "Error parsing lock address")
	}
	if !hasPort {
		return errors.New("The environment variable PCAS_LOCK_ADDRESS must be set to the address of the pcas lock server, in the form hostname:port")
	}
	return nil
}
