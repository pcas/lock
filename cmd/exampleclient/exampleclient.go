// Exampleclient is a simple client for the pcas lockd server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/lock/lockd"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"sync"
	"time"
)

func dance(ctx context.Context, name string, c *lockd.Client, wg *sync.WaitGroup) error {
	// Update the wait group
	wg.Add(1)
	defer wg.Done()
	// Acquire a lock
	L, err := c.Acquire(ctx, name, time.Minute)
	if err != nil {
		return err
	}
	// Sleep
	<-time.After(10 * time.Second)
	// Release the lock
	return L.Unlock(ctx)
}

func main() {
	// Parse the options
	opts := setOptions()
	defer cleanup.Run()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, log.Log())
	// Connect to the lockd server
	target := fmt.Sprintf("%s:%d", opts.Host, opts.Port)
	c, err := lockd.NewClient(ctx, target)
	if err != nil {
		panic(err)
	}
	cleanup.Add(c.Close)
	// Set some workers doing a dance
	wg := &sync.WaitGroup{}
	for i := 0; i < 10; i++ {
		go dance(context.Background(), "fiddlesticks", c, wg)
		<-time.After(2 * time.Second)
	}
	// Wait for the workers to exit
	wg.Wait()
}
