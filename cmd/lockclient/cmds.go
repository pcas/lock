// Cmds executes and prints commands for the user.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/lock"
	"bitbucket.org/pcas/lock/lockd"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/rgeoghegan/tabulate"
	"os"
	"time"
)

// CmdFunc describes a command function.
type CmdFunc func(ctx context.Context, ac *lockd.AdminClient, opts *Options) error

// CmdMap is the map of command names to command functions.
var CmdMap = map[string]CmdFunc{
	"acquire": acquire,
	"refresh": refresh,
	"release": release,
	"locks":   locks,
	"waiting": waiting,
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// outputJSON outputs the given data in JSON format to os.Stdout.
func outputJSON(x interface{}) error {
	b, err := json.Marshal(x)
	if err != nil {
		return err
	}
	var out bytes.Buffer
	json.Indent(&out, b, " ", " ")
	if _, err := out.WriteString("\n"); err != nil {
		return err
	}
	out.WriteTo(os.Stdout)
	return nil
}

// outputTable outputs the given data as a table to os.Stdout.
func outputTable(x interface{}, headers []string) error {
	layout := &tabulate.Layout{
		Headers: headers,
		Format:  tabulate.SimpleFormat,
	}
	asText, err := tabulate.Tabulate(x, layout)
	if err != nil {
		return err
	}
	fmt.Print(asText)
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Command functions
/////////////////////////////////////////////////////////////////////////

// acquire attempts to acquire a lock.
func acquire(ctx context.Context, ac *lockd.AdminClient, opts *Options) error {
	// Extract the name
	name := opts.CmdArgs[0]
	// Attempt to acquire the lock
	key, expires, err := ac.Acquire(ctx, name, time.Hour)
	if err != nil {
		return err
	}
	s := &lock.Metadata{
		Name:    name,
		Key:     key,
		Expires: expires,
	}
	// Should we output the data in JSON or as a table?
	if opts.JSON {
		return outputJSON(s)
	}
	return outputTable([]*lock.Metadata{s}, []string{"Name", "Key", "Expires"})
}

// refresh attempts to refresh a lock.
func refresh(ctx context.Context, ac *lockd.AdminClient, opts *Options) error {
	// Extract the name and key
	name := opts.CmdArgs[0]
	key, err := lock.KeyFromString(opts.CmdArgs[1])
	if err != nil {
		return err
	}
	// Attempt to refresh the lock
	key, expires, err := ac.Refresh(ctx, name, key)
	if err != nil {
		return err
	}
	s := &lock.Metadata{
		Name:    name,
		Key:     key,
		Expires: expires,
	}
	// Should we output the data in JSON or as a table?
	if opts.JSON {
		return outputJSON(s)
	}
	return outputTable([]*lock.Metadata{s}, []string{"Name", "Key", "Expires"})
}

// release attempts to release a lock.
func release(ctx context.Context, ac *lockd.AdminClient, opts *Options) error {
	// Extract the name and key
	name := opts.CmdArgs[0]
	key, err := lock.KeyFromString(opts.CmdArgs[1])
	if err != nil {
		return err
	}
	// Attempt to release the lock
	return ac.Release(ctx, name, key)
}

// locks prints the data returned by the List command.
func locks(ctx context.Context, ac *lockd.AdminClient, opts *Options) error {
	// Fetch the slice of active locks from the server
	S, err := ac.List(ctx)
	if err != nil {
		return err
	}
	// Should we output the data in JSON or as a table?
	if opts.JSON {
		return outputJSON(S)
	}
	return outputTable(S, []string{"Name", "Key", "Expires"})
}

// waiting prints the data returned by the Active command.
func waiting(ctx context.Context, ac *lockd.AdminClient, opts *Options) error {
	// Fetch the slice of client data from the server
	S, err := ac.Active(ctx)
	if err != nil {
		return err
	}
	// Should we output the data in JSON or as a table?
	if opts.JSON {
		return outputJSON(S)
	}
	return outputTable(S, []string{"Addr", "Timestamp", "Name"})
}
