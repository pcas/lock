// Config.go handles configuration and logging for lockclient.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/config"
	"bitbucket.org/pcas/config/env"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/log"
	"fmt"
	"github.com/pkg/errors"
	"os"
)

// Options describes the options.
type Options struct {
	Host    string   // The hostname of the lock server
	Port    int      // The port number of the lock server
	JSON    bool     // Should we output results in JSON format?
	Cmd     string   // The command
	CmdArgs []string // The command arguments
}

// Name is the name of the executable.
const Name = "lockclient"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := &Options{}
	// Parse the optional flags
	parseFlags(opts)
	// pull in and validate the configuration information
	assertNoErr(validateEnvironment(opts))
	// Parse the command and any command arguments
	assertNoErr(parseCommand(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "lockclient: %s\n", err)
		os.Exit(1)
	}
}

// parseFlags parses the command-line flags.
func parseFlags(opts *Options) {
	var verbose bool
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf(`%s provides access to the lockd server.

Usage: %s [flags] cmd [cmd args]

Commands:
  acquire name        Acquire the named lock.
  refresh name key    Refresh the named lock using key.
  release name key    Release the named lock using key.
  locks               List all currently held locks.
  waiting             List all clients waiting to acquire a lock.`, Name, Name))
	flag.SetName("Optional flags")
	flag.Add(
		flag.Bool("v", &verbose, verbose, "Increase verbosity", ""),
		flag.Bool("json", &opts.JSON, opts.JSON, "Output data in JSON format", ""),
	)
	flag.SetUsageFooter(`The following environment variables must be set:
		
  PCAS_LOCK_ADDRESS   The address of the pcas lock server, in the form hostname:port.`)
	// Parse the flags
	flag.Parse()
	// Set the logger
	if verbose {
		log.SetLogger(log.Stderr)
	}
}

// validateEnvironment pulls in configuration information from the environment and validates it.
func validateEnvironment(opts *Options) error {
	// Read in the environment configuration information
	cfg := env.EnvConfig(nil)
	// Validate the address of the lock server
	address, err := config.GetStringOrError(cfg, "lock.address")
	if err != nil {
		return errors.New("The environment variable PCAS_LOCK_ADDRESS must be set to the address of the pcas lock server")
	}
	var hasPort bool
	hasPort, opts.Host, opts.Port, err = flag.ParseAddress(address)
	if err != nil {
		return errors.Wrap(err, "Error parsing lock address")
	}
	if !hasPort {
		return errors.New("The environment variable PCAS_LOCK_ADDRESS must be set to the address of the pcas lock server, in the form hostname:port")
	}
	return nil
}

// parseCommand parses the command-line command and any command arguments.
func parseCommand(opts *Options) error {
	// Check that a command has been specified
	if flag.NArg() == 0 {
		return errors.New("The command must be specified")
	}
	// Make a note of the command and any arguments
	cmd := flag.Arg(0)
	cmdargs := flag.Args()[1:]
	// Validate the command and its arguments
	switch cmd {
	case "acquire":
		if len(cmdargs) != 1 {
			return errors.New(cmd + ": Requires lock name")
		}
	case "release", "refresh":
		if len(cmdargs) != 2 {
			return errors.New(cmd + ": Requires lock name and key")
		}
	case "locks", "waiting":
		if len(cmdargs) != 0 {
			return errors.New(cmd + ": Does not take additional command arguments")
		}
	default:
		return errors.New("Unknown command: " + cmd)
	}
	// Save the command and its arguments
	opts.Cmd = cmd
	opts.CmdArgs = cmdargs
	return nil
}
