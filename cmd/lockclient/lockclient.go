// lockclient provides command-line access to the lockd lock server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/lock/lockd"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer cleanup.Run()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Connect to the lockd server
	var ac *lockd.AdminClient
	target := fmt.Sprintf("%s:%d", opts.Host, opts.Port)
	if ac, err = lockd.NewAdminClient(ctx, target); err != nil {
		return
	}
	// Defer closing the connection
	defer func() {
		if closeErr := ac.Close(); err == nil {
			err = closeErr
		}
	}()
	// Recover and log any panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("Panic in main: %v", e)
			}
		}
	}()
	// Run the command
	if cmd, ok := CmdMap[opts.Cmd]; !ok {
		err = fmt.Errorf("Unknown command: %s", opts.Cmd)
	} else {
		err = cmd(ctx, ac, opts)
	}
	return
}

func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
