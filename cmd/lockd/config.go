// Config.go handles configuration and logging for the lockd server

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/lock/lockd"
	"bitbucket.org/pcas/logger/logd"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcastools/flag"
	"fmt"
	"github.com/pkg/errors"
	"math"
	"os"
)

// Options describes the options.
type Options struct {
	Hostname          string // The hostname to bind to
	Port              int    // The port to bind to
	MaxNumConnections int    // The maximum number of connections
}

// Name is the name of the executable.
const Name = "lockd"

// The default values for the arguments
const (
	DefaultMaxNumConnections = 1024
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse and validate the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	return &Options{
		Port:              lockd.DefaultPort,
		MaxNumConnections: DefaultMaxNumConnections,
	}
}

// parseArgs parses the command-line flags and environment variables.
func parseArgs(opts *Options) error {
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf(`%s is the pcas lock server.

Usage: %s [options]`, Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.Address("address", &opts.Hostname, &opts.Port, opts.Hostname, opts.Port, "The address to bind to, in hostname:port format", ""),
		flag.Int("max-num-connections", &opts.MaxNumConnections, opts.MaxNumConnections, "The maximum number of connections", ""),
	)
	// Propagate the application name to the logger
	flag.AddSet(logdflag.StandardSet(logd.DefaultConfig(), Name))
	// Parse the flags
	flag.Parse()
	// Sanity check
	if opts.MaxNumConnections <= 0 || opts.MaxNumConnections > math.MaxInt32 {
		return errors.New("Invalid maximum number of connections")
	}
	return nil
}
