// lockd implements a lightweight lock server

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/lock/lockd"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/netutil"
	"context"
	"fmt"
	"net"
	"os"
	"strconv"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createListener returns a new listener.
func createListener(opts *Options, lg log.Interface) (net.Listener, error) {
	// Provide some logging feedback
	lg.Printf("Starting listener on tcp://%s:%d", opts.Hostname, opts.Port)
	// Resolve the address
	addr, err := net.ResolveTCPAddr("tcp", opts.Hostname+":"+strconv.Itoa(opts.Port))
	if err != nil {
		lg.Printf("Unable to resolve address: %s", err)
		return nil, err
	}
	// Establish a connection
	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		lg.Printf("Error starting listener: %s", err)
		return nil, err
	}
	lg.Printf("Listening on %s", addr)
	// Return the listener
	return netutil.LimitListener(l, opts.MaxNumConnections), nil
}

// run starts up and runs the server. Use the given context to shutdown.
func run(ctx context.Context, opts *Options, lg log.Interface) error {
	// Start the listener
	l, err := createListener(opts, lg)
	if err != nil {
		return err
	}
	defer l.Close()
	// Create the server
	s := lockd.NewServer()
	s.SetLogger(log.PrefixWith(lg, "[lockdserver]"))
	defer s.GracefulStop()
	// Listen and handle connections
	errc := make(chan error, 1)
	go func() {
		if err := s.Serve(l); err != nil {
			errc <- err
		}
		close(errc)
	}()
	// Wait for the context to fire, or for the listener to error
	select {
	case <-ctx.Done():
		err = ctx.Err()
	case err = <-errc:
	}
	return err
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() (err error) {
	// Defer running cleanup functions
	defer cleanup.Run()
	// Parse the options and make a note of the logger
	opts := setOptions()
	lg := log.Log()
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, lg)
	// Recover and log any server panics
	defer func() {
		if e := recover(); e != nil {
			lg.Printf("Panic in main: %v", e)
			if err == nil {
				err = fmt.Errorf("Panic in main: %v", e)
			}
		}
	}()
	// Run the server
	err = run(ctx, opts, lg)
	return
}

// main
func main() {
	err := runMain()
	if err == context.Canceled || err == context.DeadlineExceeded {
		os.Exit(1)
	}
	assertNoErr(err)
}
