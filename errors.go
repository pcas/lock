// Errors defines errors for the lock package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lock

import (
	"github.com/pkg/errors"
)

var (
	ErrIncorrectKey = errors.New("Incorrect key provided")
)
