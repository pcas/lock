module bitbucket.org/pcas/lock

go 1.13

require (
	bitbucket.org/pcas/config v1.0.1
	bitbucket.org/pcas/logger v0.1.9
	bitbucket.org/pcas/metrics v0.1.8
	bitbucket.org/pcastools/cleanup v1.0.1
	bitbucket.org/pcastools/fatal v1.0.1
	bitbucket.org/pcastools/flag v0.0.6
	bitbucket.org/pcastools/grpcutil v1.0.2
	bitbucket.org/pcastools/hash v1.0.1
	bitbucket.org/pcastools/log v1.0.2
	bitbucket.org/pcastools/netutil v1.0.1
	github.com/golang/protobuf v1.3.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.1.0
	github.com/petar/GoLLRB v0.0.0-20190514000832-33fb24c13b99
	github.com/pkg/errors v0.8.1
	github.com/rgeoghegan/tabulate v0.0.0-20170527210506-f65c88667bb4
	github.com/satori/go.uuid v1.2.0
	google.golang.org/grpc v1.25.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
