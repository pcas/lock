// Key defines keys, are used by the client to confirm ownership of a lock.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lock

import (
	"encoding/json"
	"github.com/satori/go.uuid"
)

// Key is a 16-byte UUID used by the client to confirm ownership of a lock.
type Key uuid.UUID

// NilKey is a nil key.
var NilKey = Key(uuid.Nil)

/////////////////////////////////////////////////////////////////////////
// Key functions
/////////////////////////////////////////////////////////////////////////

// NewKey returns a new key.
func NewKey() Key {
	return Key(uuid.NewV1())
}

// KeyFromString returns a key from the given string.
func KeyFromString(s string) (Key, error) {
	k, err := uuid.FromString(s)
	if err != nil {
		return NilKey, err
	}
	return Key(k), nil
}

// KeyFromBytes returns a key from the given slice of bytes. It will return error if the slice isn't 16 bytes long.
func KeyFromBytes(b []byte) (Key, error) {
	k, err := uuid.FromBytes(b)
	if err != nil {
		return NilKey, err
	}
	return Key(k), nil
}

// IsEqualTo returns true iff the key is equal to the given key.
func (k Key) IsEqualTo(key Key) bool {
	return uuid.Equal(uuid.UUID(k), uuid.UUID(key))
}

// Bytes returns the bytes of the key.
func (k Key) Bytes() []byte {
	return uuid.UUID(k).Bytes()
}

// String returns a string description of the key.
func (k Key) String() string {
	return uuid.UUID(k).String()
}

// UnmarshalJSON attempts to unmarshal the given JSON into k.
func (k *Key) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	key, err := KeyFromString(s)
	*k = key
	return err
}

// MarshalJSON returns the key in JSON format.
func (k Key) MarshalJSON() ([]byte, error) {
	return json.Marshal(k.String())
}
