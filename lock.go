// Lock describes the client's view of a lock.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lock

import (
	"context"
	"sync/atomic"
	"time"
)

// Lock describes a lock.
type Lock struct {
	name     string      // The name associated with this lock
	isLocked int32       // Locked iff isLocked != 0
	stop     stopFunc    // The stop function of the lock worker
	release  releaseFunc // The function to be called to release the lock
	refresh  refreshFunc // The function called to refresh the lock
}

// WithLockFunc represents a function to be run whilst a lock is held.  The context ctx will fire if and only if we lose the lock.
type WithLockFunc func(ctx context.Context) error

// DefaultLockLifetime is the default duration for which a lock is valid.
var DefaultLockLifetime = 15 * time.Minute

// workerState describes the final state of the refresh worker.
type workerState struct {
	Key Key   // The key (if any)
	Err error // The error (if any)
}

// stopFunc stops a refresh worker, returning the current key for the lock.
type stopFunc func(ctx context.Context) (Key, error)

// releaseFunc releases the lock
type releaseFunc func(context.Context, string, Key) error

// refreshFunc refreshes the lock
type refreshFunc func(context.Context, string, Key) (Key, time.Time, error)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// refreshDuration returns how long we should wait before refreshing the lock with the server.
func refreshDuration(expires time.Time) time.Duration {
	d := time.Until(expires)
	// If the duration is at least six minutes, refresh with three minutes spare
	if d >= 6*time.Minute {
		return d - 3*time.Minute
	}
	// If the duration is at least two minutes, refresh with one minute spare
	if d >= 2*time.Minute {
		return d - time.Minute
	}
	// This doesn't look good, and presumably our clock is wrong. We return a
	// refresh duration of one minute and simply cross our fingers.
	return time.Minute
}

// worker is the background worker that refreshes a lock. To gracefully exit the worker, close the doneC channel. To force the worker to exit, fire the context.
func worker(ctx context.Context, doneC <-chan struct{}, name string, key Key, expiry time.Time, refresh refreshFunc) (Key, error) {
	// Loop until something goes wrong
	var err error
	for err == nil {
		select {
		case <-doneC:
			return key, nil
		case <-ctx.Done():
			return key, nil
		case <-time.After(refreshDuration(expiry)):
			key, expiry, err = refresh(ctx, name, key)
		}
	}
	return NilKey, err
}

// startWorker starts a background worker running refreshing the lock. Call the returned stop function to halt the worker.
func startWorker(name string, key Key, expiry time.Time, refresh refreshFunc) stopFunc {
	// Create a new context for the worker
	ctx, cancel := context.WithCancel(context.Background())
	// Start the worker running
	doneC := make(chan struct{})
	resC := make(chan *workerState, 1)
	go func() {
		key, err := worker(ctx, doneC, name, key, expiry, refresh)
		cancel()
		resC <- &workerState{Key: key, Err: err}
		close(resC)
	}()
	// Return the stop function
	return func(ctx context.Context) (Key, error) {
		close(doneC)
		var state *workerState
		select {
		case <-ctx.Done():
			cancel()
			state = <-resC
			if state.Err == context.Canceled {
				return NilKey, ctx.Err()
			}
		case state = <-resC:
		}
		return state.Key, state.Err
	}
}

//////////////////////////////////////////////////////////////////////
// Lock functions
//////////////////////////////////////////////////////////////////////

// New returns a new Lock with given name, key, expiry time, refresh function, and release function.
func New(name string, key Key, expiry time.Time, refresh refreshFunc, release releaseFunc) *Lock {
	// Start the background refresh worker and return
	return &Lock{
		name:     name,
		isLocked: 1,
		stop:     startWorker(name, key, expiry, refresh),
		release:  release,
		refresh:  refresh,
	}
}

// Name returns the name associated with this lock.
func (L *Lock) Name() string {
	if L == nil {
		return ""
	}
	return L.name
}

// Unlock releases the hold on the lock. Attempting to unlock an already unlocked lock will panic.
func (L *Lock) Unlock(ctx context.Context) error {
	// Sanity check
	if L == nil || atomic.SwapInt32(&L.isLocked, 0) == 0 {
		panic("Unlock of an unlocked lock")
	}
	// Stop the background worker and attempt to release the lock
	key, err := L.stop(ctx)
	if err == nil {
		err = L.release(ctx, L.name, key)
	}
	return err
}
