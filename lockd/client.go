// Client describes the client's view of the lock server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lockd

import (
	"bitbucket.org/pcas/lock"
	"bitbucket.org/pcas/lock/lockd/internal/lockrpc"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/grpcmetrics"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/grpcutil/grpcsnappy"
	"bitbucket.org/pcastools/log"
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"io"
	"os"
	"time"
)

// Client is the client view of the server.  It exposes (only) the Acquire method of the underlying gRPC server.  For a client view of the server that exposes more methods, see AdminClient.
type Client struct {
	ac *AdminClient
}

// AdminClient is the client view of the server.  It exposes all methods that the underlying gRPC client supports.
type AdminClient struct {
	log.BasicLogable
	metrics.BasicMetricsable
	io.Closer
	client lockrpc.LockClient // The gRPC client
}

/////////////////////////////////////////////////////////////////////////
// Client functions
/////////////////////////////////////////////////////////////////////////

// NewClient returns a new client view of the server using the given target.
func NewClient(ctx context.Context, target string) (*Client, error) {
	ac, err := NewAdminClient(ctx, target)
	if err != nil {
		return nil, err
	}
	return &Client{ac: ac}, nil
}

// Acquire acquires a lock with the given name and duration.  To cancel a request, cancel the context ctx.  You must set a Deadline on ctx, to avoid blocking forever or needlessly consuming resources on the lockd server.  If a lock is successfully returned, you must release the lock when finished with it by calling  its Release method.
func (c *Client) Acquire(ctx context.Context, name string, dur time.Duration) (*lock.Lock, error) {
	key, expiry, err := c.ac.Acquire(ctx, name, dur)
	if err != nil {
		return nil, err
	}
	return lock.New(name, key, expiry, c.ac.Refresh, c.ac.Release), nil
}

// Close closes the connection to the lock server.
func (c *Client) Close() error {
	return c.ac.Close()
}

// SetLogger sets the logger.
func (c *Client) SetLogger(l log.Interface) {
	c.ac.SetLogger(l)
}

// Log returns the logger.
func (c *Client) Log() log.Interface {
	return c.ac.Log()
}

// SetMetrics sets the metrics endpoint.
func (c *Client) SetMetrics(m metrics.Interface) {
	c.ac.SetMetrics(m)
}

// Metrics returns the metrics interface.
func (c *Client) Metrics() metrics.Interface {
	return c.ac.Metrics()
}

/////////////////////////////////////////////////////////////////////////
// AdminClient functions
/////////////////////////////////////////////////////////////////////////

// NewAdminClient returns a new client view of the server using the given target.
func NewAdminClient(ctx context.Context, target string) (*AdminClient, error) {
	// Create a new client
	c := &AdminClient{}
	// Build a connection to the gRPC server
	conn, err := grpc.DialContext(ctx, target,
		grpc.WithDefaultCallOptions(
			grpc.UseCompressor(grpcsnappy.Name),
		),
		grpc.WithChainUnaryInterceptor(
			grpclog.UnaryClientInterceptor(c.Log()),
			grpcmetrics.UnaryClientInterceptor(c.Metrics()),
		),
		grpc.WithChainStreamInterceptor(
			grpclog.StreamClientInterceptor(c.Log()),
			grpcmetrics.StreamClientInterceptor(c.Metrics()),
		),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}
	// Save the connection on the client and return
	c.Closer = conn
	c.client = lockrpc.NewLockClient(conn)
	return c, nil
}

// Acquire acquires a lock with the given name for the given duration, returning the key for the lock and the expiry time.  To cancel a request, cancel the context ctx.  You must set a Deadline on ctx, to avoid blocking forever or needlessly consuming resources on the lockd server.  If a lock is successfully returned, you must release the lock when finished with it by calling  c.Release with the given key.
func (c *AdminClient) Acquire(ctx context.Context, name string, dur time.Duration) (lock.Key, time.Time, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return lock.NilKey, time.Time{}, errors.New("Uninitialised client")
	}
	// Add the hostname to the context, so we can keep track of clients waiting for locks
	hostname, err := os.Hostname()
	if err != nil {
		return lock.NilKey, time.Time{}, err
	}
	ctx = metadata.AppendToOutgoingContext(ctx, "hostname", hostname)
	// Try to acquire the lock
	kt, err := c.client.Acquire(ctx, lockrpc.FromNameAndDuration(name, dur))
	if err != nil {
		return lock.NilKey, time.Time{}, err
	}
	// Unpack the key and expiry time and return
	return lockrpc.ToKeyAndTime(kt)
}

// Refresh refreshes the lock with the given name and key, returning the new key for the lock and the new expiry time.  To cancel a request, cancel the context ctx.  You must set a Deadline on ctx, to avoid blocking forever or needlessly consuming resources on the lockd server.  If a lock is successfully returned, you must release the lock when finished with it by calling c.Release with the given key.
func (c *AdminClient) Refresh(ctx context.Context, name string, key lock.Key) (lock.Key, time.Time, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return lock.NilKey, time.Time{}, errors.New("Uninitialised client")
	}
	// Try to refresh the lock
	kt, err := c.client.Refresh(ctx, lockrpc.FromNameAndKey(name, key))
	if err != nil {
		return lock.NilKey, time.Time{}, err
	}
	// Unpack the new key and expiry time and return
	return lockrpc.ToKeyAndTime(kt)
}

// Release releases the lock with the given name and key.  To cancel a request, cancel the context ctx.  You must set a Deadline on ctx, to avoid blocking forever or needlessly consuming resources on the lockd server.
func (c *AdminClient) Release(ctx context.Context, name string, key lock.Key) error {
	// Sanity check
	if c == nil || c.client == nil {
		return errors.New("Uninitialised client")
	}
	// Try to release the lock
	_, err := c.client.Release(ctx, lockrpc.FromNameAndKey(name, key))
	return err
}

// List fetches the metadata for the currently active locks.
func (c *AdminClient) List(ctx context.Context) ([]*lock.Metadata, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return nil, errors.New("Uninitialised client")
	}
	// Ask the lock server for the active locks
	stream, err := c.client.List(ctx, &empty.Empty{})
	if err != nil {
		return nil, err
	}
	// Extract the lock metadata
	results := make([]*lock.Metadata, 0, 10)
	for {
		// Read from the stream
		lm, err := stream.Recv()
		if err != nil {
			break
		}
		// Convert what we read
		x, err := lockrpc.FromLockMetadata(lm)
		if err != nil {
			break
		}
		results = append(results, x)
	}
	// check that nothing went wrong
	if err != nil && err != io.EOF {
		return nil, err
	}
	return results, nil
}

// Active fetches the metadata for all clients that are currently waiting for locks on the server.
func (c *AdminClient) Active(ctx context.Context) ([]*lock.ClientMetadata, error) {
	// Sanity check
	if c == nil || c.client == nil {
		return nil, errors.New("Uninitialised client")
	}
	// Ask the lock server for the waiting clients
	stream, err := c.client.Active(ctx, &empty.Empty{})
	if err != nil {
		return nil, err
	}
	// Extract the client metadata
	results := make([]*lock.ClientMetadata, 0, 10)
	for {
		// Read from the stream
		cm, err := stream.Recv()
		if err != nil {
			break
		}
		// Convert what we read
		x, err := lockrpc.FromClientMetadata(cm)
		if err != nil {
			break
		}
		results = append(results, x)
	}
	// check that nothing went wrong
	if err != nil && err != io.EOF {
		return nil, err
	}
	return results, nil
}
