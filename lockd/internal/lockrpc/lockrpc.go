//go:generate protoc -I=../proto/ --go_out=plugins=grpc:. lock.proto

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lockrpc

import (
	"bitbucket.org/pcas/lock"
	"github.com/golang/protobuf/ptypes"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// FromNameAndKey converts the given name and lock.Key to a *NameAndKey.
func FromNameAndKey(name string, key lock.Key) *NameAndKey {
	return &NameAndKey{Name: name, Key: key.String()}
}

// ToNameAndKey converts the given *NameAndKey to a string and a lock.Key.
func ToNameAndKey(nk *NameAndKey) (string, lock.Key, error) {
	k, err := lock.KeyFromString(nk.Key)
	if err != nil {
		return "", lock.NilKey, err
	}
	return nk.Name, k, nil
}

// FromNameAndDuration converts the given name and duration to a *NameAndDuration.
func FromNameAndDuration(name string, dur time.Duration) *NameAndDuration {
	return &NameAndDuration{Name: name, Duration: ptypes.DurationProto(dur)}
}

// ToNameAndDuration converts the given *NameAndDuration to a string and a time.Duration.
func ToNameAndDuration(nd *NameAndDuration) (string, time.Duration, error) {
	dur, err := ptypes.Duration(nd.Duration)
	if err != nil {
		return "", 0, err
	}
	return nd.Name, dur, nil
}

// FromKeyAndTime converts the given lock.Key and time.Time to a *KeyAndTime.
func FromKeyAndTime(key lock.Key, t time.Time) (*KeyAndTime, error) {
	ts, err := ptypes.TimestampProto(t)
	if err != nil {
		return nil, err
	}
	return &KeyAndTime{Key: key.String(), Time: ts}, nil
}

// ToKeyAndTime converts the given *KeyAndTime to a lock.Key and a time.Time.
func ToKeyAndTime(kt *KeyAndTime) (lock.Key, time.Time, error) {
	t, err := ptypes.Timestamp(kt.Time)
	if err != nil {
		return lock.NilKey, time.Time{}, err
	}
	k, err := lock.KeyFromString(kt.Key)
	if err != nil {
		return lock.NilKey, time.Time{}, err
	}
	return k, t, nil
}

// ToLockMetadata converts the *lock.LockMetadata to a *LockMetadata.
func ToLockMetadata(lm *lock.Metadata) (*LockMetadata, error) {
	ts, err := ptypes.TimestampProto(lm.Expires)
	if err != nil {
		return nil, err
	}
	return &LockMetadata{Name: lm.Name, Key: lm.Key.String(), Expires: ts}, nil
}

// FromLockMetadata converts the given *LockMetadata to a *lock.Metadata
func FromLockMetadata(lm *LockMetadata) (*lock.Metadata, error) {
	expires, err := ptypes.Timestamp(lm.Expires)
	if err != nil {
		return nil, err
	}
	k, err := lock.KeyFromString(lm.Key)
	if err != nil {
		return nil, err
	}
	result := &lock.Metadata{
		Name:    lm.Name,
		Key:     k,
		Expires: expires,
	}
	return result, nil
}

// ToClientMetadata converts the given *lock.ClientMetadata to a *ClientMetadata.
func ToClientMetadata(cm *lock.ClientMetadata) (*ClientMetadata, error) {
	ts, err := ptypes.TimestampProto(cm.Timestamp)
	if err != nil {
		return nil, err
	}
	return &ClientMetadata{Hostname: cm.Hostname, Timestamp: ts, Name: cm.Name}, nil
}

// FromClientMetadata converts the given *ClientMetadata to a *lock.ClientMetadata
func FromClientMetadata(cm *ClientMetadata) (*lock.ClientMetadata, error) {
	ts, err := ptypes.Timestamp(cm.Timestamp)
	if err != nil {
		return nil, err
	}
	result := &lock.ClientMetadata{
		Hostname:  cm.Hostname,
		Timestamp: ts,
		Name:      cm.Name,
	}
	return result, nil
}
