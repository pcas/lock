// Lock describes a lock.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lock

import (
	"bitbucket.org/pcas/lock"
	"bitbucket.org/pcas/lock/lockd/lock/internal/timeoutworker"
	"bitbucket.org/pcastools/fatal"
	"context"
	"sync"
	"time"
)

// FreeFunc is a function that will be called when the lock data transitions from being locked, or from having a non-zero reference count, to being both unlocked and with a reference count of zero. This will be called *outside* of the lock data's controlling mutex, and so the reference count or lock state may have changed when executed. The free function should make use of the lock data's ExecIfFree method to conditionally run code within the controlling mutex.
type FreeFunc func(name string)

// Lock described lock data
type Lock struct {
	name     string                  // The lock name
	w        *timeoutworker.Schedule // The scheduler for this lock
	c        chan bool               // Channel indicates the lock is free
	m        sync.Mutex              // A mutex protecting the data below
	isLocked bool                    // Are we locked?
	key      lock.Key                // The key associated with the lock
	refCount int                     // Reference count for this lock
	expires  time.Time               // The expiry time for this key
	f        FreeFunc                // The free function (if any)
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// isMetadataValid returns true iff given lock metadata validates. In particular, if the data describes a lock that has expired, it will return false.
func isMetadataValid(s *lock.Metadata) bool {
	// TODO revert to the commented out line
	//	return s != nil && s.Key != common.NilKey && s.Expires.After(time.Now())
	return s != nil && s.Key != lock.NilKey
}

//////////////////////////////////////////////////////////////////////
// Lock functions
//////////////////////////////////////////////////////////////////////

// New returns a new lock with given name, to be handled by the given scheduler.
func New(w *timeoutworker.Schedule, name string) *Lock {
	d := &Lock{
		name: name,
		w:    w,
		c:    make(chan bool, 1),
		key:  lock.NilKey,
	}
	d.c <- true
	return d
}

// FromMetadata returns a new lock populated with the given metadata, to be handled by the given scheduler.
func FromMetadata(w *timeoutworker.Schedule, s *lock.Metadata) *Lock {
	// Is there anything to do?
	if s == nil {
		return New(w, "")
	} else if !isMetadataValid(s) {
		return New(w, s.Name)
	}
	// Create the lock
	d := &Lock{
		name:     s.Name,
		w:        w,
		c:        make(chan bool, 1),
		isLocked: true,
		key:      s.Key,
		expires:  s.Expires,
	}
	// Register with the scheduler before returning the lock
	w.Insert(s.Expires, d)
	return d
}

// AddFreeFunc adds a free function to this lock data.
func (d *Lock) AddFreeFunc(f FreeFunc) {
	if f != nil {
		d.m.Lock()
		if d.f == nil {
			d.f = f
		} else {
			g := d.f
			d.f = func(name string) {
				g(name)
				f(name)
			}
		}
		d.m.Unlock()
	}
}

// ExecIfFree will call the given function if and only if the lock data is currently unlocked and has zero reference count. The function will be executed within the mutex controlling the lock data, and so should *not* attempt to access the lock's data (this will deadlock). Returns true iff the function was called.
func (d *Lock) ExecIfFree(f func()) bool {
	if f == nil {
		return false
	}
	d.m.Lock()
	ok := !d.isLocked && d.refCount == 0
	if ok {
		f()
	}
	d.m.Unlock()
	return ok
}

// IncrementRefCount increments the reference count for this lock data. Returns the updated reference count.
func (d *Lock) IncrementRefCount() int {
	d.m.Lock()
	d.refCount++
	rc := d.refCount
	d.m.Unlock()
	return rc
}

// DecrementRefCount decrements the reference count for this lock data. Returns the updated reference count. This will panic if there are more calls to DecrementRefCount than to IncrementRefCount.
func (d *Lock) DecrementRefCount() int {
	d.m.Lock()
	d.refCount--
	rc, isLocked, f := d.refCount, d.isLocked, d.f
	d.m.Unlock()
	// Sanity check on the reference count
	if rc < 0 {
		panic("Negative reference count")
	}
	// If the reference count has become zero, and if the lock is unlocked, call
	// the free function (if any)
	if rc == 0 && !isLocked && f != nil {
		f(d.Name())
	}
	return rc
}

// Metadata returns the lock metadata.
func (d *Lock) Metadata() (s *lock.Metadata) {
	d.m.Lock()
	if d.isLocked {
		// The lock is locked, so populate the metadata
		s = &lock.Metadata{
			Name:    d.name,
			Key:     d.key,
			Expires: d.expires,
		}
	} else {
		// The lock is unlocked, so set only a minimal amount of metadata
		s = &lock.Metadata{
			Name: d.name,
			Key:  lock.NilKey,
		}
	}
	d.m.Unlock()
	return
}

// Name returns the name associated with the lock data.
func (d *Lock) Name() string {
	return d.name
}

// ExpiryTime returns the expiry time of the lock.
func (d *Lock) ExpiryTime() time.Time {
	d.m.Lock()
	expires := d.expires
	d.m.Unlock()
	return expires
}

// Lock attempts to acquire the lock. Upon successfully acquiring a lock, the key that proved ownership will be returned. The lock must be refreshed within the duration given by lifetime, or it will become stale. Use the (optional) context to cancel an attempt to acquire the lock.
func (d *Lock) Lock(ctx context.Context, lifetime time.Duration) (lock.Key, error) {
	// Wait for the lock to become free, or for the context to say we're done
	select {
	case <-ctx.Done():
		return lock.NilKey, ctx.Err()
	case <-d.c:
	}
	// Sanity check
	d.m.Lock()
	if d.isLocked {
		d.m.Unlock()
		panic(fatal.ImpossibleToGetHeref("Lock is already locked")) // This should never happen
	} else if d.refCount == 0 {
		d.m.Unlock()
		panic(fatal.ImpossibleToGetHeref("Reference count is zero")) // This should never happen
	}
	// Mark the lock as owned
	d.isLocked = true
	// Generate a new key
	k := lock.NewKey()
	d.key = k
	// Set the expiry timestamp and register with the scheduler
	d.expires = time.Now().Add(lifetime)
	d.w.Insert(d.expires, d)
	// Return the key
	d.m.Unlock()
	return k, nil
}

// Refresh tries to refresh the lock using the given key. A new key will be generated and returned on success, and the expiry timestamp will be updated. If the key is incorrect, lock.ErrIncorrectKey will be returned. The lock must be refreshed within the duration given by lifetime, or it will become stale.
func (d *Lock) Refresh(k lock.Key, lifetime time.Duration) (lock.Key, error) {
	// Sanity check
	if k == lock.NilKey {
		return lock.NilKey, lock.ErrIncorrectKey
	}
	d.m.Lock()
	if !d.key.IsEqualTo(k) {
		d.m.Unlock()
		return lock.NilKey, lock.ErrIncorrectKey
	} else if !d.isLocked {
		d.m.Unlock()
		panic(fatal.ImpossibleToGetHeref("Lock is not locked")) // This should never happen
	} else if d.refCount == 0 {
		d.m.Unlock()
		panic(fatal.ImpossibleToGetHeref("Reference count is zero")) // This should never happen
	}
	// Generate a new key
	k = lock.NewKey()
	d.key = k
	// Update the expiry timestamp and reregister with the scheduler
	d.w.Remove(d.expires, d)
	d.expires = time.Now().Add(lifetime)
	d.w.Insert(d.expires, d)
	// Return the key
	d.m.Unlock()
	return k, nil
}

// checkExpired checks whether the lock has expired and, if so, updates the lock's state. Indented to only be called by the scheduler.
func (d *Lock) checkExpired() {
	// Is there anything to do?
	d.m.Lock()
	if !d.isLocked || d.expires.After(time.Now()) {
		d.m.Unlock()
		return
	}
	// Mark the lock as free
	d.isLocked = false
	// Clear the key
	d.key = lock.NilKey
	// Note the reference count and free function
	rc, f := d.refCount, d.f
	// Let other users know the lock is free
	d.c <- true
	d.m.Unlock()
	// If the reference count is zero, call the free function (if any)
	if rc == 0 && f != nil {
		f(d.Name())
	}
	return
}

// Unlock tries to unlock the lock using the key k. If the key is incorrect, lock.ErrIncorrectKey will be returned.
func (d *Lock) Unlock(k lock.Key) error {
	// Sanity check
	if k == lock.NilKey {
		return lock.ErrIncorrectKey
	}
	d.m.Lock()
	if !d.key.IsEqualTo(k) {
		d.m.Unlock()
		return lock.ErrIncorrectKey
	} else if !d.isLocked {
		d.m.Unlock()
		panic(fatal.ImpossibleToGetHeref("Lock is already unlocked")) // This should never happen
	}
	// Note: We don't check that the reference count is non-zero, since this
	// can be called from the scheduler (which doesn't increment the reference
	// count). Remove from the scheduler
	d.w.Remove(d.expires, d)
	// Mark the lock as free
	d.isLocked = false
	// Clear the key
	d.key = lock.NilKey
	// Note the reference count and free function
	rc, f := d.refCount, d.f
	// Let other users know the lock is free
	d.c <- true
	d.m.Unlock()
	// If the reference count is zero, call the free function (if any)
	if rc == 0 && f != nil {
		f(d.Name())
	}
	return nil
}
