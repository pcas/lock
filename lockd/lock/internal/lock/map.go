// Map describes a map for the lock data.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lock

import (
	"bitbucket.org/pcas/lock"
	"bitbucket.org/pcas/lock/lockd/lock/internal/timeoutworker"
	"bitbucket.org/pcas/lock/lockd/lock/lockiter"
	"sync"
)

// Map describes a map containing locks.
type Map struct {
	m     sync.RWMutex
	locks map[string]*Lock
	w     *timeoutworker.Schedule
}

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// callback is the callback function for the scheduler. It will coerce x to a Lock, and then check whether it has expired.
func callback(x interface{}) {
	if d, ok := x.(*Lock); ok {
		d.checkExpired()
	}
}

// less is the less-than function for the scheduler. It will coerce x and y to Locks, and then determine the inequality based on the lock names.
func less(x interface{}, y interface{}) bool {
	xd, ok := x.(*Lock)
	if !ok {
		return false
	}
	yd, ok := y.(*Lock)
	if !ok {
		return false
	}
	return xd.Name() < yd.Name()
}

//////////////////////////////////////////////////////////////////////
// Map functions
//////////////////////////////////////////////////////////////////////

// Len returns the number of locks in the map.
func (lm *Map) Len() int {
	lm.m.RLock()
	n := len(lm.locks)
	lm.m.RUnlock()
	return n
}

// Metadata returns the lock metadata associated with the given name from the map, if any.
func (lm *Map) Metadata(name string) *lock.Metadata {
	// Fetch the lock metadata
	var s *lock.Metadata
	lm.m.RLock()
	if d, ok := lm.locks[name]; ok {
		s = d.Metadata()
	}
	lm.m.RUnlock()
	// Check whether the metadata validates before returning
	if isMetadataValid(s) {
		return s
	}
	return nil
}

// Fetch returns the lock associated with the given name from the map, creating a new lock if necessary. Every call to Fetch should be balanced with a call to Close on the returned lock.
func (lm *Map) Fetch(name string) *Lock {
	// Check for the lock data in the map
	lm.m.RLock()
	if d, ok := lm.locks[name]; ok {
		d.IncrementRefCount()
		lm.m.RUnlock()
		return d
	}
	// No such luck -- move to a write lock
	lm.m.RUnlock()
	lm.m.Lock()
	// Recheck for the lock data in the map
	d, ok := lm.locks[name]
	if !ok {
		// No, it still doesn't exist; first check the map is allocated
		if lm.locks == nil {
			lm.locks = make(map[string]*Lock, 100)
			lm.w = timeoutworker.New(callback, less)
		}
		// Now create the lock data and add it to the map
		d = New(lm.w, name)
		d.AddFreeFunc(lm.deleteLock)
		lm.locks[name] = d
	}
	d.IncrementRefCount()
	// Release the write lock and return the lock
	lm.m.Unlock()
	return d
}

// deleteLock deletes the lock data associated with the given name from the map (if present).
func (lm *Map) deleteLock(name string) {
	lm.m.Lock()
	if d, ok := lm.locks[name]; ok {
		d.ExecIfFree(func() {
			delete(lm.locks, name)
		})
	}
	lm.m.Unlock()
}

// Names returns a slice containing the names of all locks currently in the map.
func (lm *Map) Names() []string {
	lm.m.RLock()
	names := make([]string, 0, len(lm.locks))
	for name := range lm.locks {
		names = append(names, name)
	}
	lm.m.RUnlock()
	return names
}

// Import merges the given lock metadata into the map. Metadata whose key is nil, or whose expiry times have passed, will be skipped. Metadata whose name already exists in the map will be skipped.
func (lm *Map) Import(it lockiter.Iterator) {
	// Acquire a write lock on the map
	lm.m.Lock()
	// Check the map is allocated
	if lm.locks == nil {
		lm.locks = make(map[string]*Lock, 100)
		lm.w = timeoutworker.New(callback, less)
	}
	// Iterator over the metadata
	for it.Next() {
		s := it.Value()
		// Is there anything to do?
		if isMetadataValid(s) {
			// Does a lock with this name already exist?
			if _, ok := lm.locks[s.Name]; !ok {
				d := FromMetadata(lm.w, s)
				d.AddFreeFunc(lm.deleteLock)
				lm.locks[s.Name] = d
			}
		}
	}
	// Release the write lock
	lm.m.Unlock()
}
