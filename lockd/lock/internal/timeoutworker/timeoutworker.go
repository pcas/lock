// Timeoutworker provides a simple way of calling a function at a specified time.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package timeoutworker

import (
	"bitbucket.org/pcas/lock/lockd/lock/internal/timestack"
	"sync"
	"time"
)

// CallbackFunc is the function that will be called on timeout.
type CallbackFunc func(x interface{})

// Schedule automates calling a callback function at certain times, with certain data.
type Schedule struct {
	f     CallbackFunc     // The user's callback function
	s     *timestack.Stack // The stack
	timec chan<- time.Time // The channel for passing timeouts to the worker
	timer *sync.Cond       // Conditional protecting the data below
	fired bool             // Set to true if the timer has fired
}

//////////////////////////////////////////////////////////////////////
// Schedule functions
//////////////////////////////////////////////////////////////////////

// New returns a new Schedule with given callback function. Note: There is currently no way to stop the background workers associated with a Schedule.
func New(f CallbackFunc, less timestack.LessFunc) *Schedule {
	// Sanity check
	if f == nil {
		panic("Illegal nil-valued CallbackFunc")
	}
	// Create the communication channel
	timec := make(chan time.Time)
	// Create the scheduler
	w := &Schedule{
		f:     f,
		s:     timestack.New(less),
		timec: timec,
		timer: sync.NewCond(&sync.Mutex{}),
	}
	// Start the workers and return
	go w.timerWorker(timec)
	go w.pruneWorker()
	return w
}

// pruneWorker is the main worker pruning the stack. Intended to be run it its own go-routine.
func (w *Schedule) pruneWorker() {
	for {
		// Wait for the condition to fire
		w.timer.L.Lock()
		for !w.fired {
			w.timer.Wait()
		}
		w.fired = false
		w.timer.L.Unlock()
		// Prune the stack, passing the items to the callback function
		for _, x := range w.s.Prune(time.Now()) {
			w.f(x)
		}
		// Update the timer
		if t := w.s.MinTime(); !t.IsZero() {
			w.timec <- t
		}
	}
}

// timerWorker is the main worker controlling timeouts. Intended to be run it its own go-routine.
func (w *Schedule) timerWorker(timec <-chan time.Time) {
	// Wait for the initial timeout
	when := <-timec
	timer := time.NewTimer(time.Until(when))
	running := true
	// Loop
	for {
		// If no timer is running, wait for a new timeout
		if !running {
			when = <-timec
			timer.Reset(time.Until(when))
			running = true
		} else {
			// Wait for either a new timeout, or for the timer to fire
			select {
			case <-timer.C:
				// Mark the timer as fired
				w.timer.L.Lock()
				w.fired = true
				w.timer.Signal()
				w.timer.L.Unlock()
				running = false
			case t := <-timec:
				if t.Before(when) {
					// Reset the timer
					timer.Stop()
					<-timer.C
					when = t
					timer.Reset(time.Until(when))
				}
			}
		}
	}
}

// Len returns the number of items in the Schedule.
func (w *Schedule) Len() (n int) {
	if w != nil && w.s != nil {
		n = w.s.Len()
	}
	return
}

// Insert adds the item x to be passed to the callback function at time t. If a matching item already exists at that time position, it will be replaced and the matching item returned.
func (w *Schedule) Insert(t time.Time, x interface{}) (match interface{}) {
	if w == nil || w.s == nil {
		panic("Uninitialised Schedule")
	}
	match = w.s.Insert(t, x)
	if w.s.MinTime().Equal(t) {
		w.timec <- t
	}
	return
}

// Remove removes and returns the item matching x at time t from the stack, or nil if no match for x was found in the stack at time t.
func (w *Schedule) Remove(t time.Time, x interface{}) (match interface{}) {
	if w != nil && w.s != nil {
		match = w.s.Remove(t, x)
	}
	return
}
