// Timestack maintains a stack of items ordered by time.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package timestack

import (
	"github.com/petar/GoLLRB/llrb"
	"sync"
	"time"
)

// LessFunc returns true iff x < y.
type LessFunc func(x interface{}, y interface{}) bool

// itemLessFunc returns true iff x < y.
type itemLessFunc func(x *item, y llrb.Item) bool

// item wraps a user-provided item, associating it with a time.
type item struct {
	lessThan itemLessFunc // The less-than function
	t        time.Time    // The time
	x        interface{}  // The user-provided item
}

// Stack is a stack of user items, ordered by time.
type Stack struct {
	pool    *sync.Pool // A pool of items for reuse
	m       sync.Mutex // Mutex protecting the following data
	entries *llrb.LLRB // The tree of entries
}

//////////////////////////////////////////////////////////////////////
// item functions
//////////////////////////////////////////////////////////////////////

// newItem returns a new item set to the given time and entry.
func newItem(s *Stack, t time.Time, x interface{}) *item {
	e := s.pool.Get().(*item)
	e.t = t
	e.x = x
	return e
}

// reuseItem adds the item to the pool for reuse.
func reuseItem(s *Stack, e *item) {
	if e != nil {
		e.x = nil
		s.pool.Put(e)
	}
}

// Unwrap returns the wrapped entry.
func (e *item) Unwrap() interface{} {
	return e.x
}

// Time returns the time of e.
func (e *item) Time() time.Time {
	return e.t
}

// Less returns true iff the time of e is before the time of d.
func (e *item) Less(d llrb.Item) bool {
	return e.lessThan(e, d)
}

//////////////////////////////////////////////////////////////////////
// Stack functions
//////////////////////////////////////////////////////////////////////

// New returns a new stack.
func New(less LessFunc) *Stack {
	// Sanity check
	if less == nil {
		panic("Illegal nil-valued LessFunc")
	}
	// Create the less-than function
	lessThan := func(e *item, y llrb.Item) bool {
		// Sanity check
		yy, ok := y.(*item)
		if !ok {
			return false
		}
		// First we compare on the times
		if t := yy.Time(); e.Time().Before(t) {
			return true
		} else if !e.Time().Equal(t) {
			return false
		}
		// Break equality with the user-provided function
		return less(e.Unwrap(), yy.Unwrap())
	}
	// Create the pool
	pool := &sync.Pool{
		New: func() interface{} {
			return &item{lessThan: lessThan}
		},
	}
	// Return the new stack
	return &Stack{
		pool:    pool,
		entries: llrb.New(),
	}
}

// Len returns the number of items in the stack.
func (s *Stack) Len() (n int) {
	if s != nil && s.entries != nil {
		s.m.Lock()
		n = s.entries.Len()
		s.m.Unlock()
	}
	return
}

// Insert inserts the item x in the stack at time t. If a matching item already exists at that time position, it will be replaced and the matching item returned.
func (s *Stack) Insert(t time.Time, x interface{}) (match interface{}) {
	// Sanity check
	if s == nil || s.entries == nil {
		panic("Uninitialised Stack")
	}
	// Insert the new item
	s.m.Lock()
	e := s.entries.ReplaceOrInsert(newItem(s, t, x))
	s.m.Unlock()
	// If an existing item was replaced, note this
	if e != nil {
		if ee, ok := e.(*item); ok {
			match = ee.Unwrap()
			reuseItem(s, ee)
		}
	}
	return
}

// Remove removes and returns the item matching x at time t from the stack, or nil if no match for x was found in the stack at time t.
func (s *Stack) Remove(t time.Time, x interface{}) (match interface{}) {
	// Sanity check
	if s == nil || s.entries == nil {
		return
	}
	// Delete the matching item (if any)
	s.m.Lock()
	e := s.entries.Delete(newItem(s, t, x))
	s.m.Unlock()
	// If an item was deleted, note this
	if e != nil {
		if ee, ok := e.(*item); ok {
			match = ee.Unwrap()
			reuseItem(s, ee)
		}
	}
	return
}

// Prune removes and returns a slice of all items in the stack with time before or equal to t.
func (s *Stack) Prune(t time.Time) (S []interface{}) {
	// Sanity check
	if s == nil || s.entries == nil {
		return
	}
	// Prune the entries
	es := make([]*item, 0)
	s.m.Lock()
	e := s.entries.Min()
	for e != nil {
		if ee, ok := e.(*item); ok {
			if !ee.Time().After(t) {
				s.entries.DeleteMin()
				es = append(es, ee)
				e = s.entries.Min()
			} else {
				e = nil
			}
		} else {
			e = s.entries.Min()
		}
	}
	s.m.Unlock()
	// Extract the user items from the pruned entries
	S = make([]interface{}, 0, len(es))
	for _, e := range es {
		S = append(S, e.Unwrap())
		reuseItem(s, e)
	}
	return
}

// Pop pops the earliest item off the stack. If the stack is empty, this returns nil.
func (s *Stack) Pop() (x interface{}) {
	if s != nil && s.entries != nil {
		s.m.Lock()
		e := s.entries.DeleteMin()
		s.m.Unlock()
		if e != nil {
			if ee, ok := e.(*item); ok {
				x = ee.Unwrap()
				reuseItem(s, ee)
			}
		}
	}
	return
}

// MinTime returns the earliest time for an item in the stack. If the stack is empty, this returns the zero time.
func (s *Stack) MinTime() (t time.Time) {
	if s != nil && s.entries != nil {
		s.m.Lock()
		if e := s.entries.Min(); e != nil {
			if ee, ok := e.(*item); ok {
				t = ee.Time()
			}
		}
		s.m.Unlock()
	}
	return
}
