// Iterator provides an iterator for the locks.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lock

import (
	"bitbucket.org/pcas/lock"
	"bitbucket.org/pcas/lock/lockd/lock/lockiter"
)

// Iterator provides iteration over all current lock data. Iterator satisfies the lockiter.Iterator interface.
type Iterator struct {
	calledNext bool           // Has the user called Next?
	mapIdx     int            // The index+1 of the current map
	idx        int            // The current position in the slice of map data
	names      []string       // The map names at the time of call
	val        *lock.Metadata // The current value
}

/////////////////////////////////////////////////////////////////////////
// Iterator functions
/////////////////////////////////////////////////////////////////////////

// Next advances the iterator and returns true iff more lock metadata is available.
func (it *Iterator) Next() bool {
	if it == nil {
		return false
	}
	it.calledNext = true
	it.val = nil
	for it.val == nil {
		for it.idx >= len(it.names)-1 {
			if it.mapIdx == numLockMaps {
				it.names = nil
				return false
			}
			it.mapIdx++
			it.names = lockMaps[it.mapIdx-1].Names()
			it.idx = -1
		}
		it.idx++
		it.val = lockMaps[it.mapIdx-1].Metadata(it.names[it.idx])
	}
	return true
}

// Value is the metadata associated with the current entry in the iterator. You must have called Next before the first call to Value.
func (it *Iterator) Value() *lock.Metadata {
	if !it.calledNext {
		panic("Calling Value before calling Next")
	}
	return it.val
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Import imports the given lock metadata. Metadata whose key is nil, or whose expiry times have passed, will be skipped. Metadata whose name already exists will be skipped.
func Import(it lockiter.Iterator) {
	panic("Not implemented")
}
