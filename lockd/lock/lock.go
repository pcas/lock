// Lock describes the server's view of a lock.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lock

import (
	common "bitbucket.org/pcas/lock"
	"bitbucket.org/pcas/lock/lockd/lock/internal/lock"
	"bitbucket.org/pcastools/hash"
	"context"
	"errors"
	"runtime"
	"time"
)

// numLockMaps defines the number of lock maps to use.
const numLockMaps = 1024

// lockMaps is a slice of length numLockMaps of lock maps.
var lockMaps []*lock.Map

// Lock describes a lock.
type Lock struct {
	d *lock.Lock // The data describing the lock
}

// Error messages.
var (
	ErrClosed = errors.New("Lock is closed")
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// init creates the lock maps.
func init() {
	lockMaps = make([]*lock.Map, 0, numLockMaps)
	for i := 0; i < numLockMaps; i++ {
		lockMaps = append(lockMaps, &lock.Map{})
	}
}

//////////////////////////////////////////////////////////////////////
// Lock functions
//////////////////////////////////////////////////////////////////////

// newLock returns a new lock wrapping the given lock data. This does NOT increment the reference count for the lock data.
func newLock(d *lock.Lock) *Lock {
	L := &Lock{d: d}
	runtime.SetFinalizer(L, func(LL *Lock) {
		LL.Close()
	})
	return L
}

// Close closes (but does not unlock) the lock. Every call to Fetch should be balanced with a call to Close.
func (L *Lock) Close() error {
	if L != nil && L.d != nil {
		L.d.DecrementRefCount()
		L.d = nil
		runtime.SetFinalizer(L, nil)
	}
	return nil
}

// Name returns the name associated with the lock.
func (L *Lock) Name() string {
	if L != nil && L.d != nil {
		return L.d.Name()
	}
	return ""
}

// ExpiryTime returns the expiry time of the lock.
func (L *Lock) ExpiryTime() time.Time {
	if L != nil && L.d != nil {
		return L.d.ExpiryTime()
	}
	return time.Time{}
}

// Lock attempts to acquire the lock. Upon successfully acquiring a lock, the key that proved ownership will be returned. The lock must be refreshed within the duration given by lifetime, or it will become stale. Use the (optional) context to cancel an attempt to acquire the lock.
func (L *Lock) Lock(ctx context.Context, lifetime time.Duration) (common.Key, error) {
	if L != nil && L.d != nil {
		return L.d.Lock(ctx, lifetime)
	}
	return common.NilKey, ErrClosed
}

// Refresh tries to refresh the lock using the given key. A new key will be generated and returned on success, and the expiry timestamp will be updated. If the key is incorrect, common.ErrIncorrectKey will be returned. The lock must be refreshed within the duration given by lifetime, or it will become stale.
func (L *Lock) Refresh(k common.Key, lifetime time.Duration) (common.Key, error) {
	if L != nil && L.d != nil {
		return L.d.Refresh(k, lifetime)
	}
	return common.NilKey, ErrClosed
}

// Unlock tries to unlock the lock using the key k. If the key is incorrect, common.ErrIncorrectKey will be returned.
func (L *Lock) Unlock(k common.Key) error {
	if L != nil && L.d != nil {
		return L.d.Unlock(k)
	}
	return ErrClosed
}

//////////////////////////////////////////////////////////////////////
// Public functions
//////////////////////////////////////////////////////////////////////

// Fetch returns a Lock with given name, creating a new lock if necessary. Every call to Fetch should be balanced with a call to Close on the returned Lock.
func Fetch(name string) *Lock {
	return newLock(lockMaps[int(hash.String(name)%numLockMaps)].Fetch(name))
}
