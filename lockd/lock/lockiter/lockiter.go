// Iterator provides an interface for iteration over lock data.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lockiter

import (
	"bitbucket.org/pcas/lock"
)

// Iterator is the interface satisfied by a lock metadata iterator.
type Iterator interface {
	Next() bool            // Next advances the iterator and returns true iff more metadata is available.
	Value() *lock.Metadata // Value is the lock metadata associated with the current entry in the iterator. You must have called Next before the first call to Value.
}
