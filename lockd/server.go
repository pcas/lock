// Server handles connections from a lockd client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lockd

import (
	baselock "bitbucket.org/pcas/lock"
	"bitbucket.org/pcas/lock/lockd/internal/lockrpc"
	"bitbucket.org/pcas/lock/lockd/lock"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/grpcmetrics"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	_ "bitbucket.org/pcastools/grpcutil/grpcsnappy" // Make Snappy available
	"bitbucket.org/pcastools/log"
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"sync"
	"time"
)

// DefaultPort is the default port for the lockd server to listen on.
const DefaultPort = 12351

// lockServer implements the tasks on the gRPC server.
type lockServer struct {
	lockrpc.UnimplementedLockServer
	log.BasicLogable
	metrics.BasicMetricsable
}

// Server handles client communication.
type Server struct {
	log.SetLoggerer
	metrics.SetMetricser
	grpcutil.Server
}

// waitingM is a mutex protecting waiting
var waitingM sync.Mutex

// waiting records the client metadata of clients that are waiting
var waiting map[*baselock.ClientMetadata]bool

// init initialises the map waiting
func init() {
	waiting = make(map[*baselock.ClientMetadata]bool)
}

/////////////////////////////////////////////////////////////////////////
// local functions
/////////////////////////////////////////////////////////////////////////

// metadataFromContextAndName returns the client metadata corresponding to the given incoming context and lock name
func metadataFromContextAndName(ctx context.Context, name string) *baselock.ClientMetadata {
	md, ok := metadata.FromIncomingContext(ctx)
	var hostname string
	if ok {
		if x := md.Get("hostname"); len(x) == 1 {
			hostname = x[0]
		} else {
			hostname = "Unknown host"
		}
	}
	return &baselock.ClientMetadata{
		Hostname:  hostname,
		Timestamp: time.Now(),
		Name:      name,
	}
}

/////////////////////////////////////////////////////////////////////////
// lockServer functions
/////////////////////////////////////////////////////////////////////////

// Acquire acquires a lock on the server.
func (s *lockServer) Acquire(ctx context.Context, req *lockrpc.NameAndDuration) (*lockrpc.KeyAndTime, error) {
	// Record that this client is waiting for a lock
	name, dur, err := lockrpc.ToNameAndDuration(req)
	if err != nil {
		return nil, err
	}
	cm := metadataFromContextAndName(ctx, name)
	waitingM.Lock()
	waiting[cm] = true
	waitingM.Unlock()
	// Remove this record once we are done waiting
	defer func() {
		waitingM.Lock()
		delete(waiting, cm)
		waitingM.Unlock()
	}()
	// Fetch the lock
	L := lock.Fetch(name)
	defer L.Close()
	// Acquire the lock
	k, err := L.Lock(ctx, dur)
	if err != nil {
		return nil, err
	}
	return lockrpc.FromKeyAndTime(k, L.ExpiryTime())
}

// Refresh refreshes a lock on the server.
func (s *lockServer) Refresh(ctx context.Context, req *lockrpc.NameAndKey) (*lockrpc.KeyAndTime, error) {
	// Grab the name and key
	name, key, err := lockrpc.ToNameAndKey(req)
	if err != nil {
		return nil, err
	}
	// Fetch the lock
	L := lock.Fetch(name)
	defer L.Close()
	// Refresh the lock
	key, err = L.Refresh(key, baselock.DefaultLockLifetime)
	if err != nil {
		return nil, err
	}
	return lockrpc.FromKeyAndTime(key, L.ExpiryTime())
}

// Release releases a lock on the server.
func (s *lockServer) Release(ctx context.Context, req *lockrpc.NameAndKey) (*empty.Empty, error) {
	// Grab the name and key
	name, key, err := lockrpc.ToNameAndKey(req)
	if err != nil {
		return nil, err
	}
	// Fetch the lock
	L := lock.Fetch(name)
	defer L.Close()
	// Release the lock
	if err := L.Unlock(key); err != nil {
		return nil, err
	}
	return &empty.Empty{}, nil
}

// List returns a stream containing the active locks on the server.
func (s *lockServer) List(_ *empty.Empty, stream lockrpc.Lock_ListServer) error {
	// Start iterating over the lock metadata
	it := &lock.Iterator{}
	for it.Next() {
		// convert the lock metadata
		lm, err := lockrpc.ToLockMetadata(it.Value())
		if err != nil {
			return err
		}
		// send the lock metadata
		if err := stream.Send(lm); err != nil {
			return err
		}
	}
	return nil
}

// Active returns a stream containing the metadata for all clients waiting for locks on the server.
func (s *lockServer) Active(_ *empty.Empty, stream lockrpc.Lock_ActiveServer) error {
	// copy the map that records waiting clients
	waitingM.Lock()
	temp := make(map[*baselock.ClientMetadata]bool, len(waiting))
	for k, v := range waiting {
		temp[k] = v
	}
	waitingM.Unlock()
	// stream the metadata
	for cm := range temp {
		// convert the client metadata
		x, err := lockrpc.ToClientMetadata(cm)
		if err != nil {
			return err
		}
		// send the client metadata
		if err := stream.Send(x); err != nil {
			return err
		}
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// Server functions
/////////////////////////////////////////////////////////////////////////

// NewServer returns a new lock server.
func NewServer() *Server {
	// Create the gRPC server and register our implementation
	m := &lockServer{}
	s := grpc.NewServer(
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpclog.UnaryServerInterceptor(m.Log()),
			grpcmetrics.UnaryServerInterceptor(m.Metrics()),
		)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpclog.StreamServerInterceptor(m.Log()),
			grpcmetrics.StreamServerInterceptor(m.Metrics()),
		)),
	)
	lockrpc.RegisterLockServer(s, m)
	// Wrap and return the server
	return &Server{
		SetLoggerer:  m,
		SetMetricser: m,
		Server:       s,
	}
}
