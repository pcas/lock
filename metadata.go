// Metadata defines data structures describing locks and clients waiting to acquire locks.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package lock

import (
	"time"
)

// Metadata describes a lock.
type Metadata struct {
	Name    string    // The lock name
	Key     Key       // The key associated with the lock
	Expires time.Time // The expiry time
}

// ClientMetadata describes a client waiting to acquire a lock.
type ClientMetadata struct {
	Hostname  string    // The client hostname
	Timestamp time.Time // The request time
	Name      string    // The lock name
}
